package com.gslab.gsc_30184.hackthon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;

public class DashboardActivity extends AppCompatActivity {

    ImageView bookImage,newspaperImage,cabImage,musciImage;
    GridView gridView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        bookImage=findViewById(R.id.books);
        newspaperImage = findViewById(R.id.newsimage);
        cabImage =findViewById(R.id.cabimage);
        musciImage=findViewById(R.id.musicimage);

        bookImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(getApplicationContext(),BookActivity.class);
                startActivity(newIntent);
            }
        });

        newspaperImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(getApplicationContext(),NewspaperActivity.class);
                startActivity(newIntent);
            }
        });

        cabImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(getApplicationContext(),CabActivity.class);
                startActivity(newIntent);
            }
        });

        musciImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(getApplicationContext(),MusicActivity.class);
                startActivity(newIntent);
            }
        });
    }
}
