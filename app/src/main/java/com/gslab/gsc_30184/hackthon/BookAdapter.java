package com.gslab.gsc_30184.hackthon;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by GSC-30184 on 23-06-2018.
 */


public class BookAdapter extends ArrayAdapter<BookModel> {
    private Context context;
    private ArrayList<BookModel> temp;
    private ArrayList<Integer> imgsrc;
    ImageView imageView;
    private TextView description, title, date;

    public BookAdapter(Context context, ArrayList<BookModel> temp,ArrayList<Integer> imgsrc) {
        super(context, R.layout.item, temp);
        this.context = context;
        this.temp = temp;
        this.imgsrc= imgsrc;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View listViewItem = inflater.inflate(R.layout.books_item, null, true);

        description = listViewItem.findViewById(R.id.author);
        title = listViewItem.findViewById(R.id.title);
        imageView= listViewItem.findViewById(R.id.imageview1);
        BookModel bookModel = temp.get(position);

        description.setText(bookModel.getAuthor());
        title.setText(bookModel.getBookName());
        imageView.setImageResource(imgsrc.get(position));
        return listViewItem;
    }
}