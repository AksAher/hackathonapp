package com.gslab.gsc_30184.hackthon;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

        Toolbar toolbar;

        private final int SPLASH_TIME =5000;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent dashoardIntent = new Intent(MainActivity.this,DashboardActivity.class);
                    startActivity(dashoardIntent);
                    finish();
                }
            },SPLASH_TIME);
        }

}
