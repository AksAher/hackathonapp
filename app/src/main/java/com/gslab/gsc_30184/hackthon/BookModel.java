package com.gslab.gsc_30184.hackthon;

/**
 * Created by GSC-30184 on 23-06-2018.
 */

public class BookModel {
    String imagePath,author,bookName;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
}
