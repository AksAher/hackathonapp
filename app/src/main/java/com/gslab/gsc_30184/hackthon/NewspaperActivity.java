package com.gslab.gsc_30184.hackthon;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.os.Handler;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NewspaperActivity extends AppCompatActivity {
    MyAdapter1 adapter;
     ListView listView;
     ArrayList<NewsModel> newsList;
     String jsonStr;
    NewsModel newsModel = new NewsModel();
    public static final String URL="https://newsapi.org/v2/top-headlines?apikey=d30b3276092e4fc191a0ef8553fcfbd0&country=US";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newspaper);
        newsList = new ArrayList<>();
        new GetDetails().execute();

        Log.d("inside","->"+newsList.size());
        listView = findViewById(R.id.listView1);
        adapter = new MyAdapter1(this, newsList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private class GetDetails extends AsyncTask<Void, Void, Void> {



        @TargetApi(Build.VERSION_CODES.O)
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            jsonStr = sh.makeServiceCall(URL);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    JSONArray array = jsonObj.getJSONArray("articles");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);

                        newsModel.setTitle(obj.getString("title"));
                        newsModel.setDecription(obj.getString("description"));
                        newsModel.setImageUrl(obj.getString("urlToImage"));
                        newsList.add(newsModel);
                    }

                } catch (final JSONException e) {
                    Log.e("inside", "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Json parsing error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } else {
                Log.e("inside", "server error");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "server error", Toast.LENGTH_LONG).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            adapter = new MyAdapter1(getApplicationContext(), newsList);
            listView.setAdapter((ListAdapter) adapter);
        }
    }

}
