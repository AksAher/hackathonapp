package com.gslab.gsc_30184.hackthon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class BookActivity extends AppCompatActivity {
    BookAdapter adapter;
    ArrayList<BookModel> bookList;
    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        ArrayList<Integer> itemsimg = new ArrayList<Integer>();
        itemsimg.add(R.drawable.half_gf);
        itemsimg.add(R.drawable.eleven);
        itemsimg.add(R.drawable.two);
        itemsimg.add(R.drawable.three);
        itemsimg.add(R.drawable.four);
        itemsimg.add(R.drawable.six);
        bookList= new ArrayList<>();

            BookModel bookModel = new BookModel();
            bookModel.setAuthor("Chetan Bhagat");
            bookModel.setBookName("Half Girlfraiend");
            bookModel.setImagePath("here");
            bookList.add(bookModel);


        BookModel bookModel1 = new BookModel();
        bookModel1.setAuthor("Chetan Bhagat");
        bookModel1.setBookName("The Indian Girl ");
        bookModel1.setImagePath("here");
        bookList.add(bookModel1);


        BookModel bookModel2 = new BookModel();
        bookModel2.setAuthor("Chetan Bhagat");
        bookModel2.setBookName("2 states");
        bookModel2.setImagePath("here");
        bookList.add(bookModel2);


        BookModel bookModel3 = new BookModel();
        bookModel3.setAuthor("Chetan Bhagat");
        bookModel3.setBookName("3 mistakes of life");
        bookModel3.setImagePath("here");
        bookList.add(bookModel3);

        BookModel bookModel31 = new BookModel();
        bookModel31.setAuthor("Shivaji Sawant");
        bookModel31.setBookName("Sreemanyogi");
        bookModel31.setImagePath("here");
        bookList.add(bookModel31);


        BookModel bookModel4 = new BookModel();
        bookModel4.setAuthor("Shivaji Sawant");
        bookModel4.setBookName("Chhava");
        bookModel4.setImagePath("here");
        bookList.add(bookModel4);
        listView=findViewById(R.id.bookListView);
        adapter = new BookAdapter(this, bookList,itemsimg);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
