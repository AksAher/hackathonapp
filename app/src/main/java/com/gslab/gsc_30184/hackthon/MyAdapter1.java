package com.gslab.gsc_30184.hackthon;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;
import com.bumptech.glide.Glide;

/**
 * Created by GSC-30184 on 23-06-2018.
 */

public class MyAdapter1 extends ArrayAdapter<NewsModel> {
    private Context context;
    private ArrayList<NewsModel> temp;
    ImageView imageView;
    private TextView description, title, date;
    final String BASE_URL ="https://newsapi.org/v2/top-headlines?apikey=d30b3276092e4fc191a0ef8553fcfbd0&country=US";

    public MyAdapter1(Context context, ArrayList<NewsModel> temp) {
        super(context, R.layout.item, temp);
        this.context = context;
        this.temp = temp;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View listViewItem = inflater.inflate(R.layout.item, null, true);

        description = listViewItem.findViewById(R.id.decription);
        title = listViewItem.findViewById(R.id.title);
        imageView= listViewItem.findViewById(R.id.imageview1);
        NewsModel newsModel = temp.get(position);

        description.setText(newsModel.getDecription());
        title.setText(newsModel.getTitle());
        Log.d("image1","image->"+newsModel.getDecription());
        Log.d("image1","image->"+newsModel.getImageUrl());
        imageView.setImageBitmap(BitmapFactory.decodeFile(newsModel.getImageUrl()));
        //Glide.with(convertView).load(newsModel.getImageUrl()).into(imageView);

        return listViewItem;
    }
}