package com.gslab.gsc_30184.hackthon;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by GSC-30184 on 23-06-2018.
 */

public class FlowerAdapter extends RecyclerView.Adapter<FlowerAdapter.Holder> {

    private final LayoutInflater mInflater;
    private List<NewsModel> mFlowerList;
    private FlowerClickListener mListener;
    private  int flag;
    private ArrayList<NewsModel> arraylist;
    public FlowerAdapter(FlowerClickListener listener, LayoutInflater inflater,ArrayList<NewsModel> newsList) {
        mListener = listener;
        mInflater = inflater;
        this.mFlowerList = newsList;
        this.arraylist = new ArrayList<NewsModel>();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(mInflater.inflate(R.layout.item, parent, false));

    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        NewsModel currFlower = mFlowerList.get(position);
        holder.title.setText(currFlower.getTitle());
        holder.description.setText(currFlower.getDecription());


        Glide.with(holder.itemView.getContext()).load(currFlower.getImageUrl()).into(holder.mPhoto);

    }

    @Override
    public int getItemCount() {
        return mFlowerList.size();
    }

    public void addFlowers(List<NewsModel> flowerResponses) {
        mFlowerList.clear();
        mFlowerList.addAll(flowerResponses);
        this.arraylist.addAll(mFlowerList);
        notifyDataSetChanged();
    }
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mFlowerList.clear();
        if (charText.length() == 0) {
            mFlowerList.addAll(arraylist);
        } else {

        }
        notifyDataSetChanged();
    }
    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mPhoto;
        private TextView title, description;

        public Holder(View itemView) {
            super(itemView);
            mPhoto = (ImageView) itemView.findViewById(R.id.imageview1);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.decription);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
          //  mListener.onClick(getLayoutPosition(), mFlowerList.get(getAdapterPosition()).getName(),String.valueOf(mFlowerList.get(getAdapterPosition()).getPrice()));
        }
    }

    public interface FlowerClickListener {

        void onClick(int position, String name,String price);
    }

}